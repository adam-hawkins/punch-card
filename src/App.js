import React from 'react';
import './App.css';
import '../public/round-pets-24px.svg'
import {Grid, Button} from '@material-ui/core/';

class Punch extends React.Component {
  render() {
    return (
      <img src={require('./round-pets-black-18/1x/round_pets_black_18dp.png')} alt='furry'></img>
    )
  }
}
class PunchCard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {punchCount: 0};
  }

  renderPunches() {
    let punches = [];
    for(let i = 0; i < this.state.punchCount; i++){
      punches.push(
      <span key={i}>
          <Punch />
      </span>
      )
    }
    return punches || null;
  }

  incrementPunches(){
    if (this.state.punchCount >= 10) {
      alert("Already at 10 punches, redeem your free lunch!");
    } else {
      this.setState({punchCount: this.state.punchCount+1})
    }
  }

  redeemLunch() {
    if (this.state.punchCount >= 10) {
      this.setState({punchCount: 0});
      alert("Congrats on the free lunch");
    } else if (this.state.punchCount === 0) {
      alert("You don't have any punches.");
    } else if (this.state.punchCount === 1) {
      alert("Sorry, you only have 1 punch");
    }
      else {
      alert("Sorry, you only have " +  this.state.punchCount + " punches");
    }
  }
  render() {
    const status = 'Casey Free Lunch Card';
    
    return (
      <Grid
      container
      spacing={0}
      direction="column"
      alignItems="center"
      justify="center"
      style={{ minHeight: '100vh', border: '2px solid black' }}
      >
       
       <div className="row">
         {status}
       </div>
        <div className="row">
          {this.renderPunches()}
        </div>
        <div className="row">
          <Button variant="outlined" color="primary" onClick={() => this.incrementPunches()}>
            Punch
          </Button>
          <Button variant="contained" color="primary" onClick={() => this.redeemLunch()}>
            Redeem
          </Button>
        </div>
      </Grid>
    );
  }
}

export default PunchCard;
